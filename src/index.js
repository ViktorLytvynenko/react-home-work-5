import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import './reset.scss';
import AppContainer from "./appContainer";
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import store from "./redux/store";

const root = ReactDOM.createRoot(document.getElementById('root'));
const setFirstStorage = (key) => {
    if (!localStorage.getItem(key)) {
        localStorage.setItem(key, JSON.stringify([]))
    }
}
setFirstStorage("wishListId")
setFirstStorage("basketListId")

root.render(
    <Provider store={store}>
        <AppContainer/>
    </Provider>
);

reportWebVitals();
